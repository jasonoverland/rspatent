/* CM      = 1.1.2    LAST_UPDATE = 1.0.0    */
/* pkt.h - Data packet header and related constant           */

#define PKTLEN      263            /* Length of a packet     */
#define PROLEN 8              /* Length of a protocol packet */
#define STX    2              /* Start of text         */
#define HDRLEN 5              /* Length of a packet header  */
#define LKHLEN 8              /* Length of a link header */
#define PKDLEN 256            /* Length of packet data field */
#define CRCLEN 2              /* Length of crc field        */
#define   PP   0              /* Protocol type         */
#define   DP   1              /* Data type             */
#define XP     2              /* No type               */

                          /* Packet types               */
#define UD1NAK 0              /* User data 1st blk: no ack  */
#define   UD1ACK    1              /* User data 1st blk: ack */
#define   ACKPKT    2              /* ack packet            */
#define   NAKCCE    3              /* Nack crc error        */
#define NAKNCC 4              /* Nack no crc error          */
#define   RXMITP    5              /* Retransmit starting at pkt */
#define   WACKPK    6              /* Wait acknowledge      */
#define   TXABOD    7              /* Transmission aborted   */
#define   UD2NAK    8              /* User Data not 1st blk: no ack*/
#define UD2NAK 9              /* User Data not 1st blk: ack */

#define   FWDY 1              /* Forward yes                */
#define   FWDN 0              /* Forward no            */

struct pkt{
     unsigned char stx;       /* STX - start of text        */
     unsigned char cnt;       /* data count-1               */
     unsigned char cnt_;      /* data count-1 complement    */
     unsigned char no;        /* packet number         */
     unsigned char type;      /* packet type           */
     unsigned char data0;          /* first data byte       */
     unsigned char crc1;           /* 1st crc field: prorocol pkt */
     unsigned char crc2;           /* 2nd crc field: protocol pkt */
};
