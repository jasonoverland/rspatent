;/* VERSION = 6.1.2    LAST_UPDATE = 5.9.0    */
;    prolog.h  02/12/86

;    standard prologue for msc 4.0 assembly code
;    offset for arguments
IFDEF     @BIGMODEL
@ab  equ  6
ELSE
@ab  equ  4
ENDIF
@msccomp equ   1

;
; be careful about global vars in asm modules referenced by C
; this prologue should set up the groups properly. Some
; configurations can cause an error of 50h bytes (= null segment size)
;
DGROUP    GROUP     _DATA,CONST,_BSS,STACK
_TEXT     SEGMENT BYTE PUBLIC 'CODE'
_TEXT     ENDS
;;;;NULL    SEGMENT PARA PUBLIC 'BEGDATA'
;;;;NULL    ENDS
_DATA     SEGMENT WORD PUBLIC 'DATA'
     ASSUME    DS:DGROUP
_DATA     ENDS
CONST     SEGMENT WORD PUBLIC 'CONST'
CONST     ENDS
_BSS SEGMENT WORD PUBLIC 'BSS'
_BSS ENDS
STACK     SEGMENT PARA STACK 'STACK'
STACK     ENDS
;
_TEXT     SEGMENT BYTE PUBLIC 'CODE'
     ASSUME    CS:_TEXT

;    END OF PROLOGUE.H
