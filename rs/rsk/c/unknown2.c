/* RSK     = 1.1.4    LAST_UPDATE = 1.0.2    */
/* 11/3/87 LB ext2type_tbl changed to convert to upper case to correct problem
        with 'object:' option in config file
        Fix result of ascbinstr to resolve warning message*/
/* 03/30/88 LB added extension 'Y  ' to table for production:E.S.*/
/**********mb************ 6/20 **********************************************/
#include <stdio.h>
#include <stdlib.h>
#include <d6cons.in>
#include <d6tps.in>
#include <d6error.in>

extern KCB     *act_kcb;

EXT2TYPE  ext2type_tbl[] =
     {
          "MAP",  0x4, 'G',       "MEN",  0x4, 'M',       "PG ",  0x4,'P',
          
          "M  ",  0x4, 'M',       "P  ",  0x4, 'P',
          
          
          "LDR",  0x8, 'L',       "HDR",  0x8, 'H',       "BDY",  0x8, 'B',
          
          "CMD",  0x8, 'D',       "B  ",  0x8, 'B',       "HLP",  0x8, 'A',
          
          "FAD",  0xE, 'N',       "WND",  0xE, 'W',       "W  ",  0xE, 'W',
          
          "PGM",  0xC, 'C',       "D  ",  0xC, 'C',       "R  ",  0xC, 'C',
          
          "S  ",  0xC, 'S',       "FMT",  0x0, 'F',       "Y  ",  0xC, 'C',
           0,       0,    0
     };

/*declarations for internal functions*/
EXT2TYPE *g_ext_tbl_entry();
/****************************************************/
/*                                */
/*   file2obj()                 MB     */
/*                                */
/* Purpose: Resolves filename into corresponding    */
/*       object I.D.                   */
/*                                */
/****************************************************/
file2obj (obj_file, obj_id)
char *obj_id;
char *obj_file;
     {
     EXT2TYPE *exttblp;                           /* matching entry */
     char *fileext;
     int numcnt, bin_occ, i;       /*LB 11/3/87*/
     for ( i = 0; i < 12; i++)     /*LB 11/3/87 conversion to upper case*/
          obj_file[i] = toupper(obj_file[i]);/*LB 11/3/87*/
     movcnt (obj_file, obj_id, 8);           /* file name minus extension */
                                                       /* fills bytes 0-7 */
     fileext = &obj_file[9];                 /* file extension begins */
     numcnt = ascbinstr(fileext+EXTLEN-1-MAXDIGITS,&bin_occ,MAXDIGITS);
                                                       /*LB 11/3/87*/
                                                       /* sets byte 12 by con */
                                   /*ascii string at bytes 10-11 in file name */
                              /*numcnt = number of ascii numeric bytes in file*/
    obj_id[11] = bin_occ;     /*set occurence byte in obj id from integer*/
                                        /*mod by LB 11/3/87 bin_occ defined & */
                                        /*search table for file extension mat */
                                        /*EXTLEN-numcnt bytes are identical*/
                                        /*extension ends with numcnt blanks*/
     exttblp= g_ext_tbl_entry(fileext,numcnt);
     
     if (exttblp == NULEXTPTR)               /*extension not in table*/
       return( -1 ) ;  /* Return error condition RDC 3/10/88  */
                 /* fatal_error(D5_PROC, FILE_EXTENSION_NOT_FOUND);*/
     
     movcnt(exttblp->ext, &obj_id[8], EXTLEN);/*fill extension into 9-11*/
     obj_id[12] = exttblp->type;             /*set byte 13*/
     
     return( 0 ) ; /* Return success condition RDC 3/10/88  */
     }

EXT2TYPE *g_ext_tbl_entry(ext,remcnt)
     char *ext;
     int remcnt;
          {
          EXT2TYPE *t = ext2type_tbl;
          char *tblext;
          for(;(tblext = t->ext) != 0; t++)
               {
               if ((cmpcnt(tblext, ext, EXTLEN-remcnt))
                    && (cmpcnt(tblext+(EXTLEN-remcnt),"  ",remcnt)))
                    {
                    return(t);
                    }
               }
          return(NULEXTPTR);


          }


ascbinstr(numstring, result, maxwidth)
     char *numstring;
     int *result;
     int maxwidth;
     {
     int digits, digitcnt, digitpower;
     *result = 0;
     for (digitcnt = 0,digits = maxwidth, digitpower = 1;
               digits; digits--,digitcnt++,digitpower *= 10)
          {
          if (chkrng(*(numstring+digits),'0','9'))  /*same as isdigit*/
                    *result += (*(numstring+digits) & 0x0f)*digitpower;
          else
               break;
               
          }
     return(digitcnt);
     }




chkrng(c,c1,c2)
unsigned char c,c1,c2;
     {
     if ( c < c1) return(0); /*false*/
     if ( c2 < c) return(0); /*false*/
     return(1);                     /*true*/
     }



/****************************************************/
/*                                */
/*   obj2file()       6-18-86   HG     */
/*                                */
/* Purpose: Resolves object ID into corresponding   */
/*       file name.                    */
/*                                */
/****************************************************/

obj2file( obj_id, obj_file )
char *obj_id;
char *obj_file;
{
    int i;
    
    strncpy( obj_file, obj_id, 8 );         /* file name minus extension */
    obj_file[8] = '.';
    strncpy( obj_file + 9, obj_id + 8, 3 ); /* extension     */
    for ( i=0; i<12; i++ )
     toupper( obj_file[i] );
     
     /* if id indicates a page template...     */
    
    if ( strncmp( obj_file + 9, "PG ", 3 ) == 0)
     obj_file[11] = obj_id[11] + 0x30;   /* occurence byte to ascii */
     else if ( strncmp( obj_id + 9, "  ", 2 )        == 0)
     convitoa( obj_id[11],obj_file+10,2);
    obj_file[12] = ' ';                     /* make it an old object id */
}

convitoa(num,destptr,width)
int num;
char *destptr;
int width;
     {
     char *loc, *p;
     char buffer[18];
     itoa(num,buffer,10);
     for(loc = buffer; *loc != 0;loc++)
          ;
     for(p = loc-width; p < buffer ; p++)
          *destptr++ = '0';
     strncpy(destptr,buffer,2);
    }

/* RDC removed functionality provided by TBOL processors * * * * *

key2obj(keyword, obj_id)
     char *keyword;
     char *obj_id;
{
     char obj_file[13];
    if (chk_kw_dir(keyword) == 1)
     {
          strncpy(obj_file, act_kcb->obj_id, 12);
          file2obj(obj_file, obj_id);
          return(GOOD);
     }
     else
     {
          return(BAD);
     }
}
* * * * * * * * ** * * * * * * * * * * * * * * * * * * * */

/****************************************************************/
/*                                      */
/*   getocode()          3-6-87                   */
/*                                      */
/* Purpose: Given extension, returns cacheing byte,         */
/*       or 'N' for un-known extension.                      */
/*                                      */
/****************************************************************/
/*external declarations*/

/* RDC removed obsolete functionality   * * * * * * * * * * * *
extern EXT2TYPE *g_ext_tbl_entry();
extern ascbinstr();


char getocode( extptr )
char *extptr;
     {
     int occur,numcnt;
     EXT2TYPE *tptr;
     
     numcnt = ascbinstr(extptr+EXTLEN-1-MAXDIGITS,&occur,MAXDIGITS);
     tptr = g_ext_tbl_entry(extptr,numcnt);
     if ( tptr != NULEXTPTR)
          return( tptr->code);
     else
     return( 'N' );
}
* * * * * * * * * * * * * * * * * * */
