/* RSK       1.1.4    LAST_UPDATE = 1.0.0    */
/********************************************************************
*
*    AD_MANAGER.C
*
* DESCRIPTION:
*      This file contains source code for AM.
*
*      The following files are related to the AM module.
*
*         AD_MANAGER.C - This file.
*         AM_EXPORT.H - Used to export definitions to other modules.
*         AM_DEFINE.H - Definitions local to the AM module.
*
*     DATE        PROGRAMMER    REASON
* --------------------------------------------------------------------
*    8/20/86     R.D.C.       ORIGINAL
*    2/17/87     R.D.C.       Added new DIA support.
*    10/1/87     LA        1. Put ad_receive() in read_ads() on
*                     debug switch.
                      2. Add routine ad_movoid() from am_test.c.
*    10/02/87    R.D.C        NOTE LA's changes prevent ad_manager from
                      operating properly in standalone mode.
                      Will only work standalone.
*
*
*
**********************************************************************/

#include <STDIO.H>
#include <MALLOC.H>
#include <MEMORY.H>
/* * * */
#include "D7DIA.IN"
#include "D7TPS.IN"
#include "D7CONS.IN"
#include "GEV_DEFS.IN"
/* * * */
#include "AM_DEFIN.IN"
#include "AM_EXPOR.IN"

/***********************************************************************
*
*        **** EXTERNALS ****
*
***********************************************************************/

/* Send a message to TPF.
unsigned char ad_send( req_code,   response, data_ptr ,     data_cnt     );
*/
unsigned char ad_send(unsigned char,unsigned char,unsigned char *,unsigned short);

/* Send a message to TPF
unsigned char send( unsigned char,
              unsigned char, (DIAPARM *), unsigned char *, unsigned short) ;
*/
unsigned char send();

/* Receive a message MCB *receive( seq_num ) */
tpf_mes *ad_receive( unsigned char ) ;
extern MCB *receive( unsigned char ) ;

void   ad_movoid( );
void   init_am( int ) ;

/* Return current status of object identified by obj_id  */
/* unsigned char in_cache( obj_id ) ;       */
/* unsigned char in_cache( oid *  ) ;       */

/***********************************************************************
* 
*     **** VARIABLE DECLARATIONS *****
*
***********************************************************************/




/********************************************************************
* 
*          **** RECEIVE SIMULATOR FOR AM ****
*
*********************************************************************
*
* ROUTINE NAME:    ad_receive( seq_number )
* 
* DESCRIPTION:
*           This routine provides the functional equivalent to the real
*           receive routine but instead of receiving data from TPF
*           it looks up and returns canned messages.
* 
* 
*     DATE           PROGRAMMER      REASON
* ---------------------------------------------------------------------
*    8/20/86        R.D.C.          ORIGINAL
* 
***********************************************************************/
#if DEBUG
tpf_mes *ad_receive( seq_num )

unsigned char seq_num ;
{
   unsigned char  *mes ;     /* pointer to a "simulated" TPF message */
   unsigned  char  *temp_ptr ;         /* temporary pointer used for   */
                        /* character transfer            */
   
   FILE  *ad_lst_file ;      /* Stream pointer to ad list file   */
   
   unsigned int j , i ,      /* counters for loops            */
          mes_len ,     /* length of TPF message in bytes     */
          num_ads ;     /* Number of ads in this message      */
          
/* Open file containing the exact same format as messages from TPF */
/* FORMAT is as follows :                         */
/*   1 byte  - DIA header ????????????                 */
/*   1 byte  - AMSG header ????????                    */
/*   1 byte  - number of leader ad ids contained in message */
/* 130 bytes - 10 , 13 byte ad ids                */

   if(( ad_lst_file = fopen( "ads.dir", "r" )) == NULL )
      {
      /* ERROR ACCESSING STAGED AD LIST      */
      return( NULL ) ;
      } ;
   
   mes = ( unsigned char * )malloc( sizeof( tpf_mes )) ;
   temp_ptr = mes ;
   
   *(mes++) =  fgetc( ad_lst_file ) ;
   *(mes) =  fgetc( ad_lst_file ) ;
   num_ads  =  *(mes++) ;
   
   /* fprintf( stdout, " There are %d ads in message." ,num_ads ) ;*/
   
   for( j = 0 ; j < num_ads ; j++ )
      {
      for( i = 0 ; i < AD_ID_LEN ; i++ )
      {
      *(mes++) = fgetc( ad_lst_file ) ;
      }
      }
   fclose( ad_lst_file ) ;
   return( (tpf_mes *)temp_ptr ) ;
   
}

#endif

/*******************************ROUTINE NAME:         req_ads()
* 
* DESCRIPTION:
*       This routine provides an interface between AM and the
*       present CM. Return a success/fail indicating if the
*       request could be made.
* 
*     DATE        PROGRAMMER    REASON
* ---------------------------------------------------------------------
*    8/20/86     R.D.C.       ORIGINAL
*    2/17/87     R.D.C.       Added new DIA support.
*
***********************************************************************/
unsigned char req_ads( )
   {
   /*      Request next set of ad ids from TPF.
    
      Returns a success/fail indicating if request could be made.
      Formats and sends message destined for TPF.
   */
   
   DIAPARM    *dia  ;      /* Pointer to dia information.     */
   
   if( nocom )
      return(5);
      else
      {
      /* Get memory for DIA information. This memory is released when
      message is build at communications layer.               */
      dia = ( DIAPARM * )malloc( sizeof( DIAPARM ) ) ;
      
      dia->fm0_fcode = AD_FC   ; /* FM0 function code          */
      dia->fm0_dmode = AD_DM   ; /* FM0 data mode              */
      dia->did[0]    = AD_DID0      ; /* FM0 destination ID     */
      dia->did[1]    = AD_DID1      ; /* FM0 destination ID     */
      dia->did[2]    = AD_DID2      ; /* FM0 destination ID     */
      dia->did[3]    = AD_DID3      ; /* FM0 destination ID     */
      dia->fm4info   = AD_FM4  ; /* FM4 information pointer (NULL)    */
      dia->fm9info   = AD_FM9  ; /* FM9 information pointer (NULL)    */
      dia->fm64info  = AD_FM64      ; /* FM64 information pointer (NULL)     */
      
      return(send((unsigned char   ) '\015',
            (unsigned char    ) TRUE,
            dia ,              /* Pointer to DIA information */
            (unsigned char * ) "\002",
            (unsigned char    ) 1 )) ;
      }
}

/********************************************************************
* 
*    **** Read ad message ***s()
*
* DESCRIPTION:
*       This routine returns a NULL indicating that no new
*       ad ids were received since it's last call.  It returns
*       a pointer to an ad message structure if new ad ids were
*       received since last call.  Note, if the request for new
*       ads gets aborted this routine will re-initiate the request.
* 
*     DATE        PROGRAMMER    REASON
* --------------------------------------------------------------------
*    8/20/86     R.D.C.       ORIGINAL
*    2/17/87     R.D.C.       Added new DIA support.
*
**********************************************************************/



tpf_mes  *read_ads( seq_num )

unsigned char seq_num ;
{
     
   /*
       Returns a NULL indicating that AD ids haven't been received from TPF
       yet or a pointer to and structure containing the ad ids received.
       
       TPF sends the ad ids in the following format :
       
            DIA header
            AMSG header
            number of leader ad-ids 1-byte
            leader ad ids 13-bytes each
       
       Ad_manager will copy the leader ad ids into ad_lst.
       
       Ad_manager only needs the ad ids the design spec predefines the
       number of leader ad ids sent by each TPF in each message.
     */
   
   MCB *mes ;                  /* Pointer to a Message Control Block */
#if DEBUG
     if ( nocom )
     return( ad_receive ( seq_num ) ) ;
     else
#endif
     {
     mes = receive (seq_num ) ;
     
     if ( mes->msg_status == NOT_ARRIVED ) /* message not arrived */
         return ( NULL ) ;
     
     if ( mes->msg_status == FATAL )       /* reception error encountered */
         /* SHOULD I SEND ANOTHER REQUEST ? */
         {
         /* If present release DIA info for this message */
         free( (char*)mes->rcv_diaptr ) ;
         return ( NULL ) ;
         }
     
     if ( mes->msg_status == OKAY)       /* GOOD MESSAGE RECEIVED */
         {
         /* If present release DIA info for this message */
         free( (char*)mes->rcv_diaptr ) ;
         return ( (tpf_mes *)mes->rcv_msg ) ;
         }
     }
     return( NULL ) ;                         /* ERROR CONDITION */
}




/**********
* ROUTINE NAME:       release_ads( pointer )
* 
* DESCRIPTION:
*       This routine will indicate to the NOM that AM is done
*       using the "new ad id buffer".
* 
*     DATE        PROGRAMMER    REASON
* ---------------------------------------------------------------------
*    8/20/86     R.D.C.       ORIGINAL
*
***********************************************************************/




void   release_ads( pointer )

struct    tpf_mes   *pointer ;
{

   /*       This will indicate to the NOM that AM has accepted the message
       identified by pointer.
   */
   
   free( (char*)pointer ) ;
   return ;
}




/*****************************************
* ROUTINE NAME:       mes2lst( ad_mes, ad_lst )
* 
* DESCRIPTION:
*      Copy the ad ids in ad_mes to the "EMPTY" locations in ad_lst.  The
*      algorithm should place the first ad in ad_mes in the first empty ad
*      location counting forward from ad_lst.current and continue forward
*      till either all ad ids have been placed or the routine wraps arround
*      to current.  Returns the number of ads placed in ad_lst.
*
* 
* 
* 
*     DATE        PROGRAMMER    REASON
* ---------------------------------------------------------------------
*    8/20/86     R.D.C.       ORIGINAL
*
***********************************************************************/

int   mes2lst( ad_mes, current )

struct tpf_mes *ad_mes ;    /* pointer to the top of the ad message buffer */
ad_lst_entry  *current ;     /* pointer to the "current ad" id ad_lst       */
{

   register int j = 0 ;        /* Declare & Initialize i to index to the first
          message in ad_mes          */

   ad_lst_entry  *i ;                  /* temp pointer          */
   unsigned char *mes_ptr ;          /* temporary pointer into message */
   
   i = current ;
   mes_ptr = (unsigned char *)ad_mes ;
   mes_ptr += 2 ;              /* point to first ad ad in message */
   
   while( j < ID_PER_MES )
   {
      BUMP_AD_PTR( i ) ;
      
      if ( current == i )                 /* Not enough room in ad_lst */
      break ;
      
      if ( i->status == EMPTY )
      {
      ad_lst.empty-- ;
      ad_movoid((oid *) mes_ptr, &i->ad_id ) ;
      mes_ptr += 13 ;       /* advance message pointer to next ad_id */
      j++ ;
      i->status = LOADED ;
      i->misses = 0 ;
      }
   }
   release_ads( ad_mes ) ;
   return ( j ) ;
   
}

/*********************************************************************
* 
*         **** Swap ad iad( ad_a, ad_b )
* 
* DESCRIPTION:
*      Swap ad id's byte by byte.
*
* 
* 
* 
*     DATE        PROGRAMMER    REASON
* ---------------------------------------------------------------------
*    8/20/86     R.D.C.       ORIGINAL
*
***********************************************************************/

void   swap_ad( ad_a, ad_b )

oid   *ad_a ,
*ad_b ;
{
   unsigned char temp ,
   *a      ,
   *b      ;
   
   unsigned char  i ;
   
   a = &ad_a->id[0] ;
   b = &ad_b->id[0] ;
   
#if  awb_ver
   printf("Swapping to find cached ad." ) ;
#endif

   i = AD_ID_LEN ;
   while ( i-- )
   {
      temp    = *a    ;
      *(a++)  = *b    ;
      *(b++)  = temp ;
   }
   ;
   return ;
}

/*********************************************************************
* 
*    **** AD MANAGER ROUTINE ****
*
**anager ( id_ptr )  
* 
* DESCRIPTION:
*       < Routine description.>
* 
*     DATE        PROGRAMMER    REASON
* ---------------------------------------------------------------------
*    8/20/86     R.D.C.       ORIGINAL
*
***********************************************************************/


ad_manager ( function, id_ptr )

int function ; /* Function code to define what action requested    */
          /*    0 - Fetch and return an object id at *id_ptr    */
          /*    1 - Fetch and don't return an object id at *id_ptr   */
          /*    2 - Return an object id at *id_ptr but don't fetch   */

oid *id_ptr ;  /* Pointer to a location to copy an object id to    */

{
   
   unsigned return_code ;   /* Return codes to indicate actions taken by AM */
   /* Return codes are as follows :
      0 - No error. / No warning.
      1 - No cached ids available returned id address is not cached.
      2 - Ad id address was taken out of order.
    10 - Id address returned is INVALID.           */
   
   /* Request for more ad ids is pending. */
   static  unsigned char  req_pend = FALSE ;
   
   static  unsigned char  adseq_num  ; /* Squence number for last TPF request */
   
   int     swap_level ;  /* counter to indicate depth of id swapping activity */
   
   ad_lst_entry *temp_ad_ptr ;
   
   tpf_mes  *ad_mes ;
   
   /* Boolean to tell if ad_manager has been intialized */
   static int  inited = FALSE ;
   
   
   
   return_code = 0 ;      /* initialize return code to "normal condition */
   
   if ( !inited )
      {
      if ( function != FETCH ) /* Makes no sense to FETCH if we aren't inited */
      {
      init_am( function ) ;
      inited = TRUE ;
      }
      else
      return( return_code ) ;
      }
   /* Check if new ad list message was received from TPF since last call.   */
   /* Don't forget read ads should be able to interpret and act on all      */
   /* return values from the "read()" routine.                              */
   if ( req_pend )
      if(NULL != (ad_mes = read_ads(adseq_num)))/* if message was received then */
      {                       /* transfer ids from message*/
      mes2lst( ad_mes, current ) ;      /* structure to ad_lst */
      req_pend = FALSE ;
      }
   
   
   /*fprintf(stdout, " There are %d empty ad_slots.\n\r",ad_lst.empty) ; */
   /*fprintf(stdout, " There are %d ads cached.\n\r",ADS_CACHED) ;       */
   
   if( function < RETID ) /* Is it OK to FETCH now ?  */
      {
      /* Cache next ad object if it's available */
      while (( ADS_CACHED <= AD_OBJ_CACHE) && ( cache->status == LOADED ))
      {
      if ( !nocom )
         Obj_fetch( &cache->ad_id ) ;
      BUMP_AD_PTR( cache )
      }
      }
      
   if ( function != FETCH )    /* Is it OK to return an AD ID ? */
      {
      if( current->status != EMPTY )              /* Is ad marked as used?  */
      {
      current->status = EMPTY   ;             /* Mark ad as used       */
      ad_lst.empty++ ;                  /* Bump empty counter     */
      }
      
      /* Bump ad_lst pointers and check if next ad has been "missing" too many
       times if it has then skip over it's entry in the queue                 */
      
      BUMP_AD_PTR( current )
      while( current->misses >= MISS_LIMIT )
         {
         /*fprintf(stdout, "MISS LIMIT EXCEEDED.\n\r");          */
         
         current->status = EMPTY   ;          /* Mark ad as used      */
         BUMP_AD_PTR( current )
         ad_lst.empty++ ;                 /* Bump empty counter    */
         }
         
      /* Verify that next ad ID has not already been used */
      if ( current->status == LOADED )
      {
      /* NEW AD ID IS AVAILABLE FOR PROCESSING    */
      
      
          /* if there is room in ad_lst and there is no request pending
          request new ad message            */
      if(( ad_lst.empty >= ID_PER_MES ) && ( !req_pend ))
         {
         adseq_num  = req_ads( ) ;
         req_pend = TRUE ;
         }
         
      /* Check if next object to be used is cached */
      /* If next object is cached allow return if it isn't take corrective
          action ,... if all else fails then return uncached ID.    */
      
      swap_level = 1 ;
      
      if ( nocom == FALSE )        /* If in NOCOM mode don't bother checking */
         {
         while ( Obj_get_status( &current->ad_id ) < 0 )
            {
            /*fprintf(stdout, "OBJECT NOT IN CACHE.\n\r" );*/
            
            current->misses++ ;
            
            if ( swap_level > SWAP_LIMIT )
            {
            /* Using un-cached ad ID's       */
            return_code = 1 ;
            break ;
            }
            
            temp_ad_ptr = current + swap_level ;
            if ( temp_ad_ptr > bottom )
            temp_ad_ptr -= AD_LST_LEN ;
            swap_ad( current, temp_ad_ptr ) ;
            swap_level++ ;
            return_code = 2 ;/* swapping occured so next ad is out of order */
            }
         }
         
      if ( current->misses != 0 )         /* ad was swapped atleast once */
         return_code = 2 ;                /* so ad is out of order      */
      
      
      }
      else
      {
      /* <<<< NO NEW AD ID  >>>  */
      return_code = 10 ;
      }
      
      /* return codes less than 10 are warnings not errors. At this time
         warning codes are not returned.              */
      if ( return_code <= 10 )
      {
      ad_movoid( &(current->ad_id), id_ptr ) ;
      status_code = return_code ;
      return_code = 0 ;
      }
      
      }
   return( return_code ) ;
   }
   
/*********************************************************************
* 
*       **** Initialize AMm()
*
* DESCRIPTION:
*       This routine initializes the ad manager.
* 
*     DATE        PROGRAMMER    REASON
* ---------------------------------------------------------------------
*    8/20/86     R.D.C.       ORIGINAL
*
***********************************************************************/

GCB   ads = { "" , 0 } ;

void   init_am( function )

int function ; /* Function code to define what action requested    */
          /*    0 - Fetch and return an object id at *id_ptr    */
          /*    1 - Fetch and don't return an object id at *id_ptr   */
          /*    2 - Return an object id at *id_ptr but don't fetch   */
   {
   unsigned char seq_num ;
   int i,j ;
   unsigned char *ad_mes,*ad_mes_temp ;
   unsigned char *temp ;
   
   /* Initialize ad list */
   for ( i=0 ; i < AD_LST_LEN ; i++ )
      {
      ad_lst.ad[i].misses = 0 ;
      ad_lst.ad[i].status = EMPTY ;
      }
   
   ad_lst.empty   = AD_LST_LEN ;
   
   /* Initialize ad_lst pointers      */
   top = ad_lst.ad ;
   bottom = top + (AD_LST_LEN - 1);
   current = bottom ;
   cache = top ;
   
   /*  fill/get first ad list  */
   if ( nocom )         /* only request ads if nocom if not nocom     */
      /* then TBOL LOGON application has already      */
      /* made the request.           */
      {
      seq_num = req_ads( ) ;
      ad_mes = (unsigned char * )read_ads(seq_num);
      }
   else
      {
      get_sys_gev( SYS_AD_LIST, &ads ) ;
      if ( ads.length == ( 2 + (ID_PER_MES * AD_ID_LEN)) )
      {
      ad_mes = (unsigned char *)malloc(2+(ID_PER_MES*AD_ID_LEN)) ;
      temp = (unsigned char *)ads.pointer ;
      ad_mes_temp = ad_mes ;
      *(ad_mes_temp++) = *(temp++) ;
      *(ad_mes_temp++) = *(temp++) ;
      for( j = 0 ; j < ID_PER_MES ; j++ )
         for( i = 0 ; i < AD_ID_LEN ; i++ )
            {
            *(ad_mes_temp++) = *(temp++) ;
            }
      }
      else
      {
      /* WE HAVE AN ERROR WHAT DO I DO */
      }
      }
   mes2lst( ad_mes, bottom ) ;
   
   /* Bump pointer so we  don't re-prefetch ads */
   if( !nocom)
      for ( i = 0 ; i < LOG_ON_PREFETCH ; i++ )
      BUMP_AD_PTR( cache ) ;
   
   
   /* Pre-fetch ad's  */
   if ( function != RETID )
      for ( i=0; i <= AD_OBJ_CACHE- LOG_ON_PREFETCH; i++ )
      {
      if ( !nocom )
         Obj_fetch( &cache->ad_id ) ;
      BUMP_AD_PTR( cache )
      }
   
   return ;
}

void ad_movoid( s_ad, d_ad )
oid *s_ad ,
    *d_ad ;
{
char  *s ,
      *d ;
int i ;

s = s_ad->id ;
d = d_ad->id ;

i = AD_ID_LEN ;

while ( i-- )
   *(d++) = *(s++) ;

return ;
}
