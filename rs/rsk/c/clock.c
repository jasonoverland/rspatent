/* RSK     = 1.1.4    LAST_UPDATE = 1.0.0    */
/*
      Code to drive the system clock, which must be kept independent of the
      PC's clock.  The time and date come down the line from TPF in the form
      of two strings.  We maintain them in our own format and do the
      reconversion only when somebody actually asks for the time.
      
      Source code involved in this clockwork:
      
         NEW CODE:   clock.c, clock.h
         UPDATED CODE:   rsboot.c, apiutil.c, smgr.c, los_stubs.c,
            awb.mak, awb_d.mak, d6mgr.mak, d6mgrdbg.mak, d6mgr.lnk,
            d6mgrdbg.lnk
      
      For help in putting this together with TBOL, see Irwin Horowitz.  For
          help with TPF, see Connie Devere.
      
      Les Hancock.  Initial code done 3/4/87.  Integration with reception
      system begun 4/7/87, mainly finished 4/15/87 (taxday).  Finally
          integrated with Irwin's logon code 4/27/87; tested in cooperation
          with Connie Hsieh 4/28/87.  Ready for delivery on that date.
          
          Error handling remains to be done.  See comment at the end of the
          following section.
      
      PROGRAMMER'S NOTES:
      
      The only access to the clock is via the function time_and_date().
      Its job is to update the global external variables time_gev and date_gev
      which contain data in string format.  (In fact time_gev and date_gev are
      the string components of the GEV structures.  See the code in apiutil.c)
      
      The startup code (rsboot.c, for example) must define the CLOCK and ALARM
      tasks.  However, they are not to be dispatched -- that happens to CLOCK
      the first time you call time_and_date(), and thereafter to ALARM the
      first time the system clock ticks.  (Note: the task numbers chosen must
      be in the range specified by MIN_TASK and MAX_TASK, or that range must
      be redefined.  See the LOS file rsystem.h.)
      
      Before the first call is made to time_and_date() somebody must put
      current values into time_gev and date_gev and set the_time.isvalid and
      the_time.was_set to zero.  Presumably it's TBOL that gets the GEV value
      from TPF (at logon), and rsboot() that sets isvalid and was_set.  As
      you might expect, time_and_date() does a switch on was_set and sets the
      clock first time around.
      
      Naturally, the first call to time_and_date() should be made as soon as
      possible fter the GEV's are told what time it is.  In other words, TBOL
      should try to get the time during logon.  (Irwin has promised to do 
      this.)
      
      Global values referenced include time_gev, date_gev, clock_icb and
      possibly the_time.  If all references to the GEV's are done through a
      single function, which is the proper way to make things work, then
      the_time can be local to that function.  At this writing that appropriate
      bottleneck is apiutil.c, q.v.  In other words, the reception system's
      only access to the time and date is via time_and_date() in apiutil.c.
      
      Work remaining: appropriate error handling in the calling function
      and throughout clock.c.  Specifically, error handling should go in around
          lines 139, 142, 184, 211, 290, and 339.
*/

#include <stdio.h>
#include <clock.h>
#include <apierr.in>
#include <error.in>
#define LINT_ARGS   1   /* enable stricter type checking */

/*
   Function declarations.
*/
extern ICB_TIME *DEQ_TASK(void);
extern int CHECK_TIMER(WORD);
extern int ENQ_TASK(WORD, WORD, ICB_TIME far *);
extern int SET_TIMER(WORD, WORD, int far *, WORD);
extern int SUSPEND(void);
extern void fatal_error();
static TIME *TIME_to_time(char *, char *, TIME *);
static WORD read_clock(ICB_TIME *, int);
static WORD reset_clock(ICB_TIME *, BYTE [], int);
static WORD set_clock(ICB_TIME *, BYTE [], int);
static char *time_to_DATE(char *, TIME *);
static char time_to_TIME(char *, TIME *);
void alarm(void);
void clock(void);
WORD time_and_date(char *, char *, ICB_TIME *, TIME *);

/*
   Globals.
   
   NB: To facilitate compilation of awb.exe (with los_stubs.c),
   we're putting clock_icb's definition in apiutil.c
   
   LH 4/7/87

ICB_TIME clock_icb;
*/
TIME the_time = {0, 0, 0, 0, 0, 0, 0, 0}; /* LH, 3/17/87 -- current time */

/*
   This task sleeps until the clock timer pops.  Its job is to hit the
   clock with an alarm signal (which causes the clock to reset itself),
   then go back to sleep.  The timer itself can't do that, it can only
   make a task dispatchable.  And it does no good to make the clock task
   dispatchable, since it may well be in the midst of some other chore.
   We've got to be able to wake up and put an ALARM on the clock's queue.
   
   The SUSPEND() is at the bottom of the loop because we start with the task
   nondispatchable.
*/
void
alarm()
{
   extern ICB_TIME clock_icb;
   ICB_TIME *DEQ_TASK();
   int ENQ_TASK();
   
   LOOP
   {
      clock_icb.ifunc = ALARM;                   /* set up an ALARM message */
      clock_icb.itype = S_REQ;                       /* synchronous request */
      clock_icb.istatus = GOOD_;                   /* current status is good */
      /* need error check for ENQ_TASK() */
      ENQ_TASK(CLOCK, 0, &clock_icb);          /* send it to the clock task */
      if ((DEQ_TASK())->istatus == NG)
         fatal_error(API_PROC, BAD_DATE_TIME);
      SUSPEND();                                             /* go to sleep */
   }
}

/*
   This is the clock.  It's a subtask initiated by the service manager
   task.  Once dispatched it SUSPENDs itself and wakes up in one of three
   possible states.  You can tell which state you have by looking at the
   synchronous message on top of your queue.
   
      1) ifunc == SET_CLOCK
      
         clock() has been asked to initialize its timer.  This timer expires
         on the hour, clearing <is_valid> and waking up clock() in ALARM mode
         (see below).
      
      2) ifunc == READ_CLOCK
      
         Interrogate the timer and use its new value to update the global
         structure <the_time>.
      
      3) ifunc == ALARM
      
         The hour has struck.  Reset the clock, checking for new day,
         month, year.

*/
void
clock()
{
   ICB_TIME *DEQ_TASK();
   ICB_TIME *icbptr;
   int ENQ_TASK();
   int timer;
   static BYTE days_in_mos[] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
   WORD caller;
   WORD read_clock();
   WORD reset_clock();
   WORD set_clock();
   
   timer = CREATE_TIMER();                        /* timer for use as clock */
   /* some error recovery would be nice */
   LOOP
   {
      /*
      Whoops, somebody woke us up.  Who?  Look at the message
      queue to find out.
       */
      if ((icbptr = DEQ_TASK())   /* if there was a message on the queue... */
        && icbptr->itype == S_REQ)
      {
         caller = icbptr->itask;
         switch (icbptr->ifunc)
         {
         case SET_CLOCK:                /* this case should occur once only */
            icbptr->istatus = set_clock(icbptr, days_in_mos, timer);
            icbptr->timeptr->was_set = 1;            /* see time_and_date() */
            break;
         case READ_CLOCK:
            icbptr->istatus = read_clock(icbptr, timer);
            break;
         case ALARM:
            icbptr->istatus = reset_clock(icbptr, days_in_mos, timer);
         }
         /*
         Send a synchronous response to caller.
          */
         icbptr->itype = S_RES;
         /* need error check for ENQ_TASK() */
         ENQ_TASK(caller, 0, icbptr);           /* unblock the calling task */
      }
      SUSPEND();                              /* till they wake us up again */
   }
}

/*
   Looking at the timer tells us how many ticks are left in the
   current hour.  Using that info, update the global <the_time>.
   The year, day and hour stay the same, of course -- they're
   updated only when the timer pops, if then.  To avoid the use
   of floating point we're working in tenths of a second, then
   rounding to minutes and seconds at the last moment.
*/
static
WORD
read_clock(icbptr, timer)
ICB_TIME *icbptr;
int timer;
{
   int CHECK_TIMER();                                       /* LOS function */
   WORD tenthts_elapsed;
   
   if (icbptr->timeptr->is_valid)             /* if 0, timer popped somehow */
   {
          /* the use of long constant and loss of precision are intentional */
      tenths_elapsed = 36000 - ((DOUBLEWORD) CHECK_TIMER(timer))
        * 100 / 91;
      icbptr->timeptr->mins = tenths_elapsed / 600;
      icbptr->timeptr->secs = tenths_elapsed % 600 / 10;
      return GOOD_;                                          /* good return */
   }
   return NG;                              /* timer popped -- it's an error */
}

/*
   The timer has popped.
   
   Bump the day of the year, the day of the month if
   appropriate, etc.  Reset the timer to pop on the next hour.
   Logic:
   
   min = 0
   sec = 0
   hour = ++hour % 24
   if (hour == 0)
   { # new day
      day of month = ++ day of month % days in month
      if (day of month == 0)
      {
         month = ++month % 12
         if (month == 0)
         {   # new year
            ++year
            if (this is now a leap year)
               days_in_mos[February] = 29
            else
               days_in_mos[February] = 28
         }
      }
   }
*/
static
WORD
reset_clock(icbptr, days_in_mos, timer)
ICB_TIME *icbptr;
BYTE days_in_mos[];
int timer;
{
   int SET_TIMER();                                         /* LOS function */
   
   icbptr->timeptr->secs = icbptr->timeptr->mins = 0;              /* given */
   if(!(icbptr->timeptr->hours = ++icbptr->timeptr->hours % 24))
      if(!(icbptr->timeptr->mday = ++icbptr->timeptr->mday %
        days_in_mos[icbptr->timeptr->mon]))
         if(!(icbptr->timeptr->mon = ++icbptr->timeptr->mon % 12))
            days_in_mos[1] = ++icbptr->timeptr->year % 4 ? 28 : 29;
/*
   Should have error checking and recovery in case of bad return from
   SET_TIMER.  (See los/inc/rserror.h.)
*/
   SET_TIMER(timer, TICKS_PER_HOUR, (int far *) &icbptr->timeptr->is_valid,
     ALARM);
   icbptr->timeptr->is_valid = 1;              /* goes to 0 when timer pops */
   return GOOD_;                                     /* always a good return */
}

/*
   Using the info in <the_time> (which must be correct -- we
   have no way to check it here), set a timer to pop on the hour.
   When it pops it wakes up the ALARM task, which in turn invokes
   the CLOCK task for resetting.
   
   What we want to know here is: how many PC ticks till
   then?  It would seem that our INT-8 handler ticks the
   timers at the rate of 9.1 per second.
   
   The number of clock ticks till the next hour is 9.1 times
   the number of seconds remaining in the hour.  That number is
   3600 - the number of seconds elapsed.  The number of seconds
   elapsed is min * 60 + sec.  So we have ticks = 91L * (3600L
   - (icbptr->timeptr->mins * 60L + icbptr->timeptr->secs))
   / 10L.  The result of this expression will always be less
   than 0x7fff, so it can be cast as a WORD.  That's good,
   since SET_TIMER() can handle only a WORDful of ticks.
   
   If timers ran on long ints we wouldn't have to go through
   any of this reset-on-the-hour stuff.
   
   Notice that we set the number of days in February before
   doing any validation of the given date, since part of the
   validation is a check for the day of the month.
*/
static
WORD
set_clock(icbptr, days_in_mos, timer)
ICB_TIME *icbptr;
BYTE days_in_mos[];
int timer;
{
   int SET_TIMER();                                         /* LOS function */
   
   /* Update table: if leap year, February has 29 days. */
   days_in_mos[1] = icbptr->timeptr->year % 4 ? 28 : 29;
   if (VALIDATE_TIME(icbptr->timeptr, days_in_mos))
   {
/*
   Should have error checking and recovery in case of bad return from
   SET_TIMER.  (See los/inc/rserror.h.)
*/
      SET_TIMER(timer, (WORD) (91L * (3600L -
        (icbptr->timeptr->mins * 60L +
        icbptr->timeptr->secs)) / 10L),
        (int far *) &icbptr->timeptr->is_valid, ALARM);
      icbptr->timeptr->is_valid = 1;           /* goes to 0 when timer pops */
      return GOOD_;                                           /* good return */
   }
   icbptr->timeptr->secs = icbptr->timeptr->mins = icbptr->timeptr->hours =
     icbptr->timeptr->year = icbptr->timeptr->mon = icbptr->timeptr->mday = 0;
   return NG;                                                 /* bad return */
}

/*
   This is an application's only access to the clock task.  If the clock
   has not yet been set, we assume we have the current time and date in
   time_gev and date_gev and proceed to set it.  If the clock has been
   set already, we get the current time and date into those gev's.
   
   Update 4/27/87, LH.  It seems that tboldrv(), in the course of
   checking operands, may call procoper(), which calls global_proc(),
   which calls get_sys_gev(), which calls time_and_date(), before
   putting its initial value into sys_table[].  In other words,
   time_and_date() may be called with null pointers for date_gev and
   time_gev.  In that case simply return with no error indication.
*/
WORD
time_and_date(date_gev, time_gev, icbptr, timeptr)
char *date_gev, *time_gev;
ICB_TIME *icbptr;
TIME *timeptr;
{
   int ENQ_TASK();
   TIME *time_to_time();
   /*
      If called with null strings for date and time, simply return.
      It's not an error -- somebody's checking to see whether there's
      anything in the system table, and there isn't.
   */
   if (date_gev == (char *) 0 || time_gev == (char *) 0)
      return GOOD_;
   icbptr->itype = S_REQ;
   icbptr->timeptr = timeptr;
   if (icbptr->timeptr->was_set)
      icbptr->ifunc = READ_CLOCK;
   else
   {
      /*
         Convert from their format to ours.  Return value not used.
      */
      TIME_to_time(date_gev, time_Gev, timeptr);
      icbptr->ifunc = SET_CLOCK;
   }
   if ((ENQ_TASK(CLOCK, 0, icbptr) == E_NONE) &&
     (DEQ_TASK())->istatus == GOOD_)
   {
      /*
         Reconversion from our format to theirs.  Return values are not used.
       */
      time_to_TIME(time_gev, timeptr);        /* convert our time to theirs */
      time_to_DATE(date_gev, timeptr);        /* convert our date to theirs */
      return GOOD_;                                          /* good return */
   }
   return NG;                                             /* ENQ_TASK error */
}

/*
   Convert TPF'S time-and-date format to our own format.
   Returns a pointer to our time-and-date record, which it also takes as
   an argument.
*/
static
TIME *
TIME_to_time(their_date, their_time, our_time)
char *their_date, *their_time;
TIME *our_time;                                    /* pointer to our record */
{
   our_time->hours = ATOI_2(their_time);
   our_time->mins = ATOI_2(their_time + 2);
   our_time->secs = ATOI_2(their_time + 4);
   our_time->mon = ATOI_2(their_date) - 1;   /* they index from 1, we from 0 */
   our_time->mday = ATOI_2(their_date + 2) - 1;
   our_time->year = ATOI_2(their_date + 4) * 100 +
     ATOI_2(their_date + 6);
   return our_time;
}

/*
   Convert the date from our format to that used by TPF.
   Return a pointer to their new string.
*/
static
char *
time_to_DATE(their_date, our_time)
char *their_date;
TIME *our_time;
{
   their_date[0] = (our_time->mon + 1) / 10 + '0';
   their_date[1] = (our_time->mon + 1) % 10 + '0';
   their_date[2] = (our_time->mday + 1) / 10 + '0';
   their_date[3] = (our_time->mday + 1) % 10 + '0';
   their_date[4] = (our_time->year) / 1000 + '0';
   their_date[5] = (our_time->year) % 1000 / 100 + '0';
   their_date[6] = (our_time->year) % 100 / 10 + '0';
   their_date[7] = (our_time->year) % 10 + '0';
   return their_date;
}

/*
   Convert the time from our format to that used by TPF.
   Return a pointer to their new string.
*/
static
char *
time_to_TIME(their_time, our_time)
char *their_time;
TIME *our_time;
{
   their_time[0] = our_time->hours / 10 + '0';
   their_time[1] = our_time->hours % 10 + '0';
   their_time[2] = our_time->mins / 10 + '0';
   their_time[3] = our_time->mins % 10 + '0';
   their_time[4] = our_time->secs / 10 + '0';
   their_time[5] = our_time->secs % 10 + '0';
   return their_time;
}
