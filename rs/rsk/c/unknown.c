                        do{
                                switch (cursor_motion) {
                                        case TAB_FORWARD:
                        fscan = fscan->next;
                            break;
                                        case TAB_BACK:
                                                fscan = fscan->previous;
                            break;
                                        case TAB_UP:
                                                fscan = fscan->t_up;
                            break;
                                        case TAB_DOWN:
                                                fscan = fscan->t_down;
                            break;
                                        case TAB_RIGHT:
                                                fscan = fscan->t_right;
                            break;
                                        case TAB_LEFT:
                                                fscan = fscan->t_left;
                               }
                }
                        while ((fscan->s & DISPLAY_FIELD) && (fscan != NULL))
                        break;

                case HOME:

            fscan = find_home();
                break;

                case LAST_FIELD:

            fscan = find_end();
                break;

                case GOTO_FIELD:

                        fscan = check_for_new_field(field_name);
                        break;

                case NEW_FIELD:

            if(!screen_cursor_flag){
                if(Op_cursor_fn){
                    cursor_control(GOTO_FIELD,Op_cursor_fn);
                    }
                else {
                    if(F->s & DISPLAY_FIELD){
                                       F = bump_cursor(F);
                       W = F->owning_window;
                        }
                        W= F->owning_window;
                                    event.event = NEW_FIELD;
                                    do_display_event(event);
                    Get_io_buffer();
                                    screen_cursor_flag = ON;
                                    Display_event = NO_EVENT;
                    }
            }
                        return;
                case OFF_CURSOR;

                        if(screen_cursor_flag){
                event.event = EXIT_INPUT_FIELD;
                do_display_event(event);
                                event.event = OFF_CURSOR;
                                do_display_event(event);
                Rel_io_buffer();
                                screen_cursor_flag = OFF;
                        }
                        Display_event = NO_EVENT;
                        return;

        }
        if(fscan != NULL){
        while(fscan->s & DISPLAY_FIELD)
            fscan = fscan->next;
                F = fscan;
                W = F->owning_window;
        }
        else {
                event.event = BEEP;
                do_display_event(event);
        }
        cursor_control(NEW_FIELD,0);
    Display_event = NO_EVENT;
}
/************************************************************************
*
*                              DEFINES
*
*************************************************************************/

#define NUMBER_OF_FIELDS 26

#define NO_LOCATION          0
#define COMMAND_BAR_LOCATION 1
#define JUMP_WINDOW_LOCATION 2
#define LOOK_FIELD_LOCATION  3
#define PAGE_LOCATION        4

#define COMMAND_BAR_ID       4
#define AD_PARTITION_ID      3
#define HEADER_ID            1

/************************************************************************
*
*                            DECLARATIONS
*
*************************************************************************/

struct field_table {
        unsigned char field_name;
        unsigned char location;
        unsigned int  function;
}FIELD_TABLE;

static struct field_table find_field[NUMBER_OF_FIELDS] = {
        'A',COMMAND_BAR_LOCATION,ACTION,
        'B',COMMAND_BAR_LOCATION,BACK_PAGE,
        'C',JUMP_WINDOW_LOCATION,SRNPRT,
        'D',NO_LOCATION,NO_EVENT,
        'E',COMMAND_BAR_LOCATION,LOGOFF,
        'F',NO_LOCATION,NO_EVENT,
        'G',JUMP_WINDOW_LOCATION,GUIDE,
        'H',COMMAND_BAR_LOCATION,PAGE_HELP,
        'I',JUMP_WINDOW_LOCATION,INDEX,
        'J',COMMAND_BAR_LOCATION,JUMP,
        'K',NO_LOCATION,NO_EVENT,
        'L',LOOK_FIELD_LOCATION,NO_EVENT,
        'M',COMMAND_BAR_LOCATION,PREVIOUS_MENU,
    'N',COMMAND_BAR_LOCATION,NEXT_PAGE,
        'O',NO_LOCATION,NO_EVENT,
        'P',COMMAND_BAR_LOCATION,PATH,
        'Q',NO_LOCATION,NO_EVENT,
        'R',NO_LOCATION,NO_EVENT,
        'S',NO_LOCATION,NO_EVENT,
        'T',JUMP_WINDOW_LOCATION,TOOLS,
        'U',JUMP_WINDOW_LOCATION,UNDO,
        'V',JUMP_WINDOW_LOCATION,VIEWPATH,
        'W',NO_LOCATION,NO_EVENT,
    'X',NO_LOCATION,NO_EVENT,
        'Y',NO_LOCATION,NO_EVENT,
        'Z',JUMP_WINDOW_LOCATION,ZIP
};
/************************************************************************
*
*                       check_for_new_field()
*
*        INPUT:  char field name
*        OUTPUT: pointer to field
*
*************************************************************************
*     DATE            AUTHOR
*************************************************************************
*    14 APRIL '86
*    PROTOTYPE
*
*    21 MAY '86         ALGIS
*    coded
*
*     6 June '86        Algis
*     Version 1.0 of keyboard manager coded but not yet tested.
*
*    10 April '87       Algis
*    Move this function out of dofunc.c to here to centralize cursor control.
*    Add new function to return the pointer to the field.
*
**********************************************************************/

static struct field FAR * check_for_new_field(key)

char key;              /* Physical key                 */

{
 
unsigned char location;
unsigned int  function;
struct field FAR *fscan,*start_field;
struct field FAR *orig_field;               /*  LRZ 10/28/87    */
struct window FAR *wscan;
unsigned int i;

/* initialize local variables */

   location = PAGE_LOCATION;
   function = 0;

   if((isdigit(key)) && (!(Op_cursor_fn))){
       key = key &0x0f;
   }
   else if((!(Op_cursor_fn)) || ((Op_cursor_fn) &&
                         (find_partition(COMMAND_BAR_ID) == Op_cursor_elem)))

           key = toupper(key);
           for(i=0;i<NUMBER_OF_FIELDS;++i){
                   if(key == find_field[i].field_name){
                           function = find_field[i].function;
                           location = find_field[0].location;
                           break;
                   }
           }
           if((function_status[function].s & DISABLE_MASK) &&
                           !(function_status[function].s & OVER_RIDE_FILTER))
                           return(NULL);
           }
   }
   if(location == NO_LOCATION){
       return(NULL);
   }
   if(location == JUMP_WINDOW_LOCATION){
       if(Jump_window &&
               P->window_stack[P->window_index].window_type!=JUMP_WINDOW){
               return(NULL);
       }
       else if (!(Jump_window)){
               do_function_event(JUMP);
               KM_navigate();
               if(!Jump_window)
                       return(NULL);
           cursor_control(OFF_CURSOR,0);
       }
       Op_cursor_fn = NULL;
       wscan = P->window_stack[P->window_index].o_window;
       fscan = search_partition(wscan,key);
       if(fscan == NULL)
           fscan = wscan->first_field;
       return(fscan);
   }

   if(location == COMMAND_BAR_LOCATION){
       Op_cursor_fn = NULL;
       wscan = find_partition(COMMAND_BAR_ID);
       if(wscan == NULL)
           return(NULL);
       fscan = search_partition(wscan,key);
       return(fscan);
   }
   if(location == LOOK_FIELD_LOCATION){
       Op_cursor_fn = NULL;
       wscan = find_partition(AD_PARTITION_ID);
       if(wscan == NULL)
                return(NULL);
       return(wscan->first_field);
   }
   if(location == PAGE_LOCATION){
       if(Op_cursor_fn){
           orig_field = start_field = Op_cursor_elem->first_field; /* LRZ */
       }
       else
       {
            orig_field = F;                             /* LRZ 10/28/87 */
            start_field = F->owning_window->first_field; /* start at the
                        first field in that partition -  LRZ 10/28/87 */
       }
       fscan = start_field;
       Op_cursor_fn = NULL;
       
       do{
           if(fscan->name == key)
                if(!(fscan->s & DISPLAY_FIELD)) /* make sure
                            this is a cursor-able field - LRZ 10/28/87 */
                    if(fscan->owning_window->a.id != AD_PARTITION_ID &&
                    fscan->owning_window->a.id != COMMAND_BAR_ID)
                        return(fscan);
           fscan = fscan->next;
       }
       while(fscan != start_field && fscan != NULL);
       
       if (fscan == start_field)    /* we've come full circle, reset the */
            fscan = orig_field;     /* cursor to the original field - LRZ */

       return(fscan);
   }
}
/************************************************************************
*
*              search_partition
*
*        INPUT: pointer to window struct, field name
*        OUTPUT: pointer to field
*
*************************************************************************
*     DATE            AUTHOR
*************************************************************************
*    10 April '87     Algis
*    Original
************************************************************************/

static struct field FAR * search_partition(wscan,key)

struct window FAR *wscan;
char key;

{

struct field FAR *fscan;

    fscan = wscan->first_field;
    if(fscan == NULL)
        return(NULL);
    while(fscan->owning_window == wscan){
        if(fscan->name == key)
            return(fscan);
        fscan = fscan->next;
    }
    return(NULL);
}

/************************************************************************
*
*              find_partition
*
*        INPUT: partition_id
*        OUTPUT: pointer to partition
*
*************************************************************************
*  DATE               AUTHOR
*************************************************************************
*  10 April '87       Algis
*  Original
************************************************************************/

static struct window FAR * find_partition(w_id)
unsigned char w_id;

{

struct field FAR *fscan;
    fscan = F;
    do{
        if(fscan->owning_window->a.id == w_id)
            return(fscan->owning_window);
        fscan = fscan->next;
    }
    while(fscan != F);
    return(NULL);
}
/************************************************************************
*
*              find_home
*
*        INPUT: none
*        OUTPUT: struct field pointer
*
*************************************************************************
*  DATE               AUTHOR
*************************************************************************
*  10 April '87       Algis
*  Original
************************************************************************/

static struct field FAR * find_home()

{
 
struct field FAR *fscan;
struct field FAR *home_field;
    home_field = F;
    fscan = F->next;
     do{
        if((!(fscan->s & DISPLAY_FIELD))&&((fscan->oy > home_field->oy) ||
           (fscan->oy == home_field->oy && fscan->ox < home_field->ox)))
         home_field = fscan;
         }
     while((fscan =fscan->next) != F);

     if(home_field->owning_window->a.id == COMMAND_BAR_ID ||
        home_field->owning_window->a.id == AD_PARTITION_ID)
            return(NULL);

     return(home_field);
}
/************************************************************************
*
*              find_end()
*
*        INPUT: none
*        OUTPUT: struct field pointer
*
*************************************************************************
*  DATE               AUTHOR
*************************************************************************
*  10 April '87       Algis
*  Original
************************************************************************/

static struct field FAR * find_end()

{
 
struct field FAR *fscan;
struct field FAR *start_field;
struct field FAR *end_field;

        if((fscan = find_home())==NULL)
        return(NULL);
    
    start_field = fscan;
    end_field = fscan;
    fscan = fscan->next;
    do{
        if(!(fscan->s & DISPLAY_FIELD)){
            if((fscan->oy < end_field->oy) ||
          (fscan->oy == end_field->oy && fscan->ox > end_field->ox )){
            if(fscan->owning_window->a.id != COMMAND_BAR_ID &&
               fscan->owning_window->a.id != AD_PARTITION_ID){
                  end_field = fscan;
            }
            }
        }
    }
    while((fscan=fscan->next) != start_field);
    return(end_field);
}
/************************************************************************
*
*              bump_cursor()
*
*        INPUT: none
*        OUTPUT: struct field pointer
*
*************************************************************************
*  DATE               AUTHOR
*************************************************************************
*  29 April '87      Algis
*  Original
************************************************************************/

static struct field FAR * bump_cursor(fscan)
struct field FAR *fscan;

{
   if(fscan->owning_window->a.id == COMMAND_BAR_ID && fscan->name == 'B' &&
     (!(fscan->previous->s & DISPLAY_FIELD))){
     return(fscan->previous);
   }
   while(fscan->s & DISPLAY_FIELD)
       fscan = fscan->next;
   return(fscan);
}
/************************************************************************
*
*              init_cursor()
*
*        INPUT: none
*        OUTPUT: struct field pointer
*
*************************************************************************
*  DATE               AUTHOR
*************************************************************************
*  10 April '87       Algis
*  Original
************************************************************************/

void init_cursor()

{
    screen_cursor_flag = OFF;
}
