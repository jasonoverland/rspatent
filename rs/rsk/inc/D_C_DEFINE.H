/* RSK     = 1.1.4    LAST_UPDATE = 1.0.0    */
/********************************************************************
*
*** Define structures and constants to be used by tracking module. **
*
*********************************************************************
*
* FILE NAME:          < D_C_DEFINE.H >
* 
* DESCRIPTION:
*            <File description>
*
*     DATE           PROGRAMMER      REASON
* ------------------------------------------------------------------- 
*    8/20/86        R.D.C.          ORIGINAL
*    2/17/87        R.D.C.          Added constant for DIA info.
*
*********************************************************************/


/* Constants for DIA info. */
#define    DC_FC     0x00
#define    DC_DM     0x00
#define    DC_DID0   0x00
#define    DC_DID1   0x00
#define    DC_DID2   0xD2
#define    DC_DID3   0x00
#define    DC_FM4    0x00
#define    DC_FM9    0x00
#define    DC_FM64   0x00



#define MAX_CLASS   32  /* Number of elements (object/function types
                      in stat_look.                        */

#define OBJ_ID_LEN  13  /* Object id length in bytes.  Presently assumed
                      to be constant but may become variable for
                      driver 7. Value = 13.                */

#define REC_LST_LEN 256 /* Length of record list in bytes.       */

#define OBJ_TYPE  0x48   /* Record code for object records        */

#define FUN_TYPE  0x49   /* Record code for function records      */

#define OBJ_REC_LEN  19 /* Length of object record in bytes.  Note
                      since object id is part of this record
                      this constant may become variant for driver 7.*/

#define FUN_REC_LEN   6 /* Length of function record in bytes.     */

#define MES_HEAD_LEN      20 /* Length of message header in bytes.     */
  
#define DATA_PACKET_SIZE 256 /*Length of communications data packet in bytes. */

#define D_C_REQ_CODE      13 /* COmmunications request code to send data
                      collection data.                */

#define ON            1

#define OFF           0

#define BIT_0  0x01
#define BIT_1  0x02
#define BIT_2  0x04
#define BIT_3  0x08
#define BIT_4  0x10
#define BIT_5  0x20
#define BIT_6  0x40
#define BIT_7  0x80


