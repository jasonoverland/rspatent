/* RSK     = 1.1.4    LAST_UPDATE = 1.1.3    */
/* VERSION = 6.1.3    LAST_UPDATE = 6.0.9    */

#include <esperr.in>

/* Define ESP error table. */

ECB  esp_err_table[] =
{
"Bad ERS response",null_rtn,
"Bad ERS return code",null_rtn,
"Bad version response",null_rtn,
"Unknown machine type",null_rtn,
"Deadman timer popped",null_rtn,
"Bad ers header",null_rtn
};
