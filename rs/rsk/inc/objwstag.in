/* RSK     = 1.1.4    LAST_UPDATE = 1.0.0    */
/* objwstag.in 11/3/87 14:50 */
/*   
     file:          objwstag.in
     author:        Michael L. Gordon
     date:          26-October-1987
     facility: Object Management Write Stage Unit

     Description:

          Public interface of the object write stage unit.

     History:

*/
#ifndef   objwstagDEF
#define objwstagDEF 1

#ifndef SYSunitDEF
#include  <sysunit.in>
#endif
#ifndef objtypesDEF
#include  <objtypes.in>
#endif
#ifndef OBJUNITDEF
#include  <objunit.in>
#endif
#ifndef objownDEF
#include <objown.in>
#endif

/* Upper bound of number of pending candidates for update */
#define   ObjStageUpperMaxPending  10

/* ObjStageProcessCandidates - process the pending list of updates */
extern    void ObjStageProcessCandidates( void );

/* function ObjStageSetMaxPending - set the maximum number of pending stage
   update operations before doing them
*/
extern    void ObjStageSetMaxPending( int );

/* function ObjNewStageInstance - Notification of a new instance of a
     stage object.
   Returns status.
*/
extern    int  ObjNewStageInstance( Objcxtrt * );

/* function ObjStageCandidacyCheck - Notification of a possible stage
   candidate.
   Returns status.
*/
extern    int  ObjStageCandidacyCheck( Objcxtrt * );

/* function ObjStageCandidateForfeitNotify - Notification that a stage
     candidate is being forfeited.
   Returns status.
*/
extern    int  ObjStageCandidateForfeitNotify( Objcxtrt * );

#endif
/* end of objwstag.in */
