/* RSK     = 1.1.4    LAST_UPDATE = 1.0.0    */
/* objunitp.in 10/28/87 9:50 */
/*
     file:          objunitp.in
     author:        Michael L. Gordon
     date:          3-June-1987
     facility: Object Management Unit

     Description:

          Private restricted interface of the object unit.

     History:

*/
#ifndef   objunitpDEF
#define objunitpDEF 1

     extern Objcxtrt     *ObjCxtBasePtr;  /* base of directory */
     extern int          curobjcount;   /* number of active entries */
     extern    int       maxobjcount;   /* max number of object entries */
     extern    int       curstreamcount;     /* number of active stream entries */
     extern    Objcontext *ObjStreamBasePtr; /* base of stream list */

     /* Request for objtreedelete handling of the root object */
     typedef enum begin
          objtreerootdelete,
          objtreerootnodelete
     end objtreerootdeletet;

/* private function objidxtoptr - objidx value is supplied,
   Function returns address of the Objcxtrt entry.
*/
extern    Objcxtrt  *objidxtoptr( Objidxt );

/* private function objptrtoidx - map objcxtprt to objidx */
extern    Objidxt        objptrtoidx( Objcxtrt * );

/* private function findrootobj - find the root object entry.
     Returns the objcxtptr of the root entry, which may be the same
     as that supplied by the caller.
*/
extern    Objcxtrt  *findrootobj( Objcxtrt * );

/* private function findobject - find the object in the directory.
   Returns pointer and objidx of the entry.
   If retcode is 0 then object was found.
   If retcode is +1 then object not found, first free entry returned.
   If retcode is -1 then object not found, directory is full.
extern    Objcxtrt  *findobject( objidptr, retcode, retObjidx )
          Objectid  *objidptr;
          int            *retcode;
          Objidxt        *retObjidx;
*/
extern    Objcxtrt  *findobject( Objectid *, int *, Objidxt * );

/* private function installobject - find or install the object in the
   directory.
   Returns pointer and objidx of the entry.
   If retcode is 0 then object was already installed.
   If retcode is +1 then object is newly installed in the directory.
   If retcode is -1 then object is not installed, directory is full.
extern    Objcxtrt  *installobject( objidptr, retcode, retObjidx )
          Objectid  *objidptr;
          int            *retcode;
          Objidxt        *retObjidx;
*/
extern    Objcxtrt  *installobject( Objectid *, int *, Objidxt * );

/* private function obufferrundown - deallocate the buffer associated
   with the cxt entry */
extern    void obufferrundown( Objcxtrt * );

/* private function objrundowncxtentry - run down the resources of the
   specified objcxtentry and then free the entry
*/
extern    void objrundowncxtentry( Objcxtrt * );

/* private function objtreedelete - Walk the family tree of this parent
     object entry recursively, deleting all inactive references including
     this one if eligible and objtreerootdeletet = objtreerootdelete.
     Returns the number of references remaining, which is 0 if the entry
     got deleted.
*/
extern int     objtreedelete( Objcxtrt *, ObjUserStatus, objtreerootdeletet );

/* private function memorycacheobject - memory cache this object */
extern    void memorycacheobject( Objcxtrt * );

#endif
/* end of objunitp.in */
