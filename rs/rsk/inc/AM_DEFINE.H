/* RSK     = 1.1.4    LAST_UPDATE = 1.0.0    */
/*********************************************************************
*
*          **** AM Module Definitions ****
*
**********************************************************************
*
* FILE NAME:          AM_DEFINE.H
*
* DESCRIPTION:
*            <File description>
*
*
*     DATE           PROGRAMMER      REASON
* --------------------------------------------------------------------
*    8/20/86        R.D.C           ORIGINAL
*    2/17/87        R.D.C           Added constant for DIA info.
*
**********************************************************************/

/***********************************************************************
*
*            **** CONSTANTS ****
*
************************************************************************/

#define LINT_ARGS 1

/* Constants for DIA info. */
#define    AD_FC     0x00
#define    AD_DM     0x20
#define    AD_DID0   0x00
#define    AD_DID1   0x00
#define    AD_DID2   0xD2
#define    AD_DID3   0x00
#define    AD_FM4    0x00
#define    AD_FM9    0x00
#define    AD_FM64   0x00

/* Length of ad ids (possible future changes include variable object ids    *
#define    AD_ID_LEN  13

/* Number of elements (ad ids) in ad_lst.  */
/* Must be > ID_PER_MES + AD_OBJ_CACHE     */
#define    AD_LST_LEN  15

/* Length of TPF response.            */
#define    ID_PER_MES  10

/* Number of ads that logon application will prefetch */
#define    LOG_ON_PREFETCH 2

/* Number of ad objects to cache.     */
/* Must be > AD_LST_LEN - ID_PER_MES       */
#define    AD_OBJ_CACHE 2

/* Status for ads in ad_lst.          */
#define    LOADED   1

/* Status for ads in ad_lst.          */
#ifndef EMPTY
    #define    EMPTY     0
#endif

/* Maximum number of swaps to attempt before returning an un-cached ad id.*/
/* Must be <= AD_OBJ_CACHE                               */
#define    SWAP_LIMIT    1

/* Maximum number of times to allow an "come up" uncached in the queue    */
/* before removing it.                                   */
#define    MISS_LIMIT    2

/* POSSIBLE OBJECT STATUS VALUES  */
#define    DONT_KNOW     0
#define    IN_CACHE 1
#define    IN_STAGE 2
#define    REQUESTED     3

/* THESE ARE TEMPORARY SHOULD COME FROM SYSTEM INCLUDE */
/* #define    FETCH 1        */
#define    TRUE     1
#define    FALSE    0



/***********************************************************************
*
*            **** DEFINITIONS ****
*
***********************************************************************/

/*     ad_lst - List of object ids for the next several ads to be presented
       to the user.  Also, some space is set aside to keep track of status
       of the ad list.                                     */

       typedef struct                /* TEMPORARY TILL CAN LINK TO OTHER*/
       {
       unsigned char id[13] ;
       } oid ;

       typedef struct              /* Ad list entry (one per id )     */
      {
      unsigned char misses ;        /* The number times id's turn came up
                           but couldn't be used because it
                           wasn't pre-fetched                 */
      unsigned char status ;        /* Status of this ad      */
      oid ad_id ;               /* Object identification   */
      } ad_lst_entry ;


/*     ** NOTE : Structure iod is from OBJECT.IN.      */

/*     tpf_mes - Message structure passed to ad manager containing new ad
       ids.
                                                  */
     typedef struct
       {
       unsigned char   ad_ids[ (ID_PER_MES*AD_ID_LEN)+2] ;
      /* action code, #of ids , leader ad ids 13-bytes each    */
       } tpf_mes ;

/***********************************************************************
*
*              **** EXTERNALS ****
*
***********************************************************************/


extern unsigned char nocom ; /* Tells if service manager is running    */
                    /* in standalone mode or not         */

/***********************************************************************
*
*           **** VARIABLE DECLARATIONS ****
*
***********************************************************************/

static    struct ad_lst
       {
       unsigned int empty ;       /* number of empty entries in ad_lst*/
       ad_lst_entry ad[AD_LST_LEN] ;    /* ad entries       */
       } ad_lst ;

static    ad_lst_entry  *current , /* current ad entry in ad list */
                *top     ,    /* top ad entry in ad list     */
                *bottom  ,    /* bottom ad entry in ad list  */
                *cache   ;    /* last cached entry in ad list*/

static    unsigned char req_pend ; /* request pending flag        */

static    unsigned status_code   ; /* status of AM just before last return */



/********************************************************************
*
*               **** MACROS ****
*
*********************************************************************

/* Increment the pointer to an ad_list_entry */
#define BUMP_AD_PTR( x )      x = ( (x) == (bottom))?(top):(++x) ;

/* The number of unused ads that have been requested to be cached by AM */
#define ADS_CACHED ((current>cache)?((cache-current)+AD_LST_LEN):(cache-curre

