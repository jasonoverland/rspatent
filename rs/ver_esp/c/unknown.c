/* VER_ESP = 6.20.5   LAST_UPDATE = 1.0.0    */

/* moved PACKAGE VERSION to this file to avoid recompilation */
/* of RSK directory when package changes */
/* PRL - 3/28/88 */

#include "VERS_NO.IN"
unsigned char rs_version[] = { VERSION_NO };         /*LB 11/30/87*/
unsigned rs_version_size = sizeof (rs_version);
