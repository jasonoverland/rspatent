;/* VERSION = 6.1.2    LAST_UPDATE = 5.9.0    */
;********************************************************************/
;*                                          */
;* IWAMISCS - miscellaneous hardware support routines           */
;*                                          */
;********************************************************************/
     include prolog.h
     public    _vstmsiz
     public    _vstfpgm
     public    _getflag
     public    _getds,_getcs,_getes
environ equ    word ptr 2Ch
_DATA     segment
     extrn     __psp:word    ;program segment prefix address(segment)
_DATA     ends

_vstmsiz proc   near
     push es
     push bx
     mov  bx,__psp
     mov  es,bx           ;es = ptr to segment prefix
     mov  ax,es:2    ;offset two = top of memory
     sub  ax,bx           ;subtract base to get size
     pop  bx
     pop  es
     ret             ;return to caller
_vstmsiz endp

ifdef     @BIGMODEL
_vstfpgm proc   far
else
_vstfpgm proc   near
endif
     push bp
     mov  bp,sp            ;access function parameter
     push es
     push bx
     mov  es,@ab[bp]       ;address of program (psp origin)
     xor  bx,bx            ;set size to 0
     mov  ah,4Ah           ;dos call = setblock
     int  21h
     jc   seterr           ;should not happen!
;                     ;find environment origin
     mov  ax,es:environ    ;environment origin pointer in psp
     mov  es,ax
     xor  bx,bx            ;now release environment
     mov  ah,4Ah           ;..with another setblock
     int  21h
     jc   seterr           ;unsuccessful result
     xor  ax,ax            ;ax = 0 on success
seterr:               ;ax = error code from setblock on erro
     pop  bx
     pop  es
     pop  bp
     ret              ;return to caller
_vstfpgm endp

ifdef     @BIGMODEL
_getds     proc      far
else
_getds     proc      near
endif
     mov  ax,ds            ;return the value
     ret
_getds     endp

;
ifdef     @BIGMODEL
_getcs     proc      far
else
_getcs     proc      near
endif
     mov  ax,cs            ;return the value
     ret
_getcs     endp

;
ifdef     @BIGMODEL
_getes     proc      far
else
_getes     proc      near
endif
     mov  ax,es            ;return the value
     ret
_getes     endp
     page
ifdef     @BIGMODEL
_getflag proc   far
else
_getflag proc   near
endif
     pushf                 ;save the flags
     pop  ax          ;..and return them as function value
     ret
_getflag endp
     include epilogue.h
;**********************************************************************/
;*     END IWAMISCS                              */
;**********************************************************************/
     end
