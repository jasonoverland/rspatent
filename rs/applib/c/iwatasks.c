/* VERSION = 6.1.2    LAST_UPDATE = 5.9.0    */
/********************************************************************/
/*                                          */
/* IWATASK  - task manipulation functions                  */
/*                                          */
/*    DESCRIPTION - this module is a collection of subroutines to*/
/*        provide support for some task services. The      */      
/*        routines are:                          */
/*          deftask - define a task to the system          */
/*          disptask - make a task dispatchable            */
/*          whotask - tell me my task number (me=active task)   */
/*                                          */
/*    SUBROUTINES -                              */
/*        INTERNAL: none except those called by user       */
/*        EXTERNAL:                              */
/*        issuesvc: executes the svc for the task service     */
/*        getflag: gets the hardware 'flags' register data    */
/*        getds: copies the current data segment           */
/*                                          */
/*    COMMENTS -                                 */
/*        1. Invalid semaphore requests abend the multitasker.   */
/*                                          */
/*********************************************************************/
#include <iwssvcsc.h>                         /*svc structure maps   */
#include <iwssvccc.h>                         /*svc constants        */
sttaic struct sfvplst *nullptr = 0;           /* null pointer    */
extern issuesvc();

/********************************************************************/
/* deftask - define a task (also called a "manager") and leave it  */
/*     unstarted (suspended). The arguments are:           */
/*     taskid - the sfv number (taskid) already assigned to task */
/*     epip/epseg - entry point for the task. IP:CS.       */
/*     environ - pointer to space for the new task's stack area. */
/*            Environ (=ds offset in small model) references a*/
/*            24-byte area with the environment to use when   */
/*            the stack is first started. See "env" structure */
/*            for its layout. The task's regular stack grows  */
/*            above the environment. The stack must be at     */
/*            least 128 bytes, with additional space depending*/
/*            on the functions used by the task.           */
/*     prty -task priority, using multitasker's priority scheme.*/
/*                                         */
/*     Deftask builds the argument list for the creation SVC (see   */
/* the documentation) and builds the structure with the register    */
/* values needed for an SVC.                          */
/*                                         */
/********************************************************************/
void deftask(taskid,epip,epseg,environ,prty)
int  taskid;        /* number of this task         */
int  epseg;         /* task entry point segment    */
int  epip;          /* task entry point instr. ptr      */
struct env *environ; /* pointer to new task stk environment */
int  prty;          /* task priority               */
{
  struct svcregs svcregs;          /* parms to svc       */
  struct sfvplst plst;
  extern int getds();
  extern int getes();
  extern int getflag();
  struct { int scs, sss, sds, ses; } rv;
  unsigned int codes, datas, stacks, extras;
    /* start code */
  plst.sf_indx = 0;           /* don't care             */
  plst.sf_prty = prty;             /* task priority      */
  plst.sf_susp = _QSUSPND;
  (struct env *)plst.sf_stkad = environ; /* stack pointer     */
#ifndef M_I86LM
  plst.sf_stkss = getds();         /* stack segment      */
#endif
  svcregs.sr_indx = taskid;
  svcregs.sr_type = _QSVCMGRMK;
  svcregs.sr_spec = 0;
  environ->en_csseg = epseg;       /* set registers      */
  environ->en_csoff = epip;
  environ->en_psw = getflag();          /* flags word         */
  if (!environ->en_ds) {    /* see if these have been set already */
     environ->en_ds = getds();     /* data segment       */
     environ->en_es = getes();
  }
                         /* other regs are left alone */
  issuesvc(&plst, &svcregs);       /* execute the svc    */
 }

/********************************************************************/
/* disptask - change the state of an existing task to dispatchable. */
/*     This makes the task eligible to start, from either its entry */
/*     point if new, or from its place of suspension if not. The    */
/*     function is a no-op if the task is already dispatchable.     */
/*     Clearly one task cannot do this to itself or it would already*/
/*     be dispatchable.                          */
/*                                          */
/*     Disptask accepts one argument, the index of the task. It     */
/*     returns nothing.                          */
/*                                          */
/*     The sr_spec value is set nonzero to force a first dispatch   */
/*     of this task if it is ready. Otherwise we can have a condi-  */
/*     tion where the highest priority dispatchable task is not     */
/*     running. That case might violate the rules          */
/********************************************************************/
void disptask(taskid)
int  taskid;        /* sfv for task to dispatch   */
{
  struct svcregs svcregs;          /* parms to svc       */
    /* start code */
  svcregs.sr_indx = taskid;
  svcregs.sr_type = _QSVCDSPT;          /* set dispatchable req   */
  svcregs.sr_spec = 1;
  issuesvc(nullptr, &svcregs);          /* execute the svc    */
 }

/********************************************************************/
/* whotask - this function asks the system the id of the active task*/
/*     and returns it as an integer to the caller. No argument is   */
/*     required. Useful if someone else creates the task and the    */
/*     task itself needs its number for other requests.         */
/*                                          */
/********************************************************************/
whotask()
{
  struct svcregs svcregs;
  svcregs.sr_indx = 0;             /* unused         */
  svcregs.sr_type = _QSVCCURSFV;   /* request = get current   */
  svcregs.sr_spec = 0;             /* unused         */
  return(issuesvc(nullptr,&svcregs));   /* svc returns the id    */
 }
