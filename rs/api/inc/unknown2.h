/* API     = 1.1.2    LAST_UPDATE = 1.0.0    */
#define OPLIMIT    0x6F         /* Number of API instructions. */

typedef struct disasm
{
unsigned char nemonic[11];
}DASM;

DASM  opname[]=
{
{"BRK       "},
{"CJEQ      "},
{"CJNE      "},
{"CJLT      "},
{"CJGT      "},
{"CJLE      "},
{"CJGE      "},
{"JMP       "},
{"DEF_FLD   "},
{"DEF_FLD   "},
{"SET_ATT   "},
{"OPEN      "},
{"CLOSE_ALL "},
{"CLOSE     "},
{"READ      "},
{"READ      "},
{"WRITE     "},
{"WRITE     "},
{"CONNECT   "},
{"DISCONNECT"},
{"SEND      "},
{"SEND      "},
{"RECEIVE   "},
{"CANCEL    "},
{"NAV       "},
{"NAV_NEXT  "},
{"NAV_BACK  "},
{"NAV_FIRST "},
{"NAV_LAST  "},
{"FETCH     "},
{"FETCHRQ   "},
{"OPENWINDOW"},
{"OPENWINDOW"},
{"CL_WINDOW "},
{"CL_WINDOW "},
{"KILL      "},
{"PURGE     "},
{"MOVE      "},
{"MOVE_ABS  "},
{"SWAP      "},
{"ADD       "},
{"SUB       "},
{"MUL       "},
{"DIV       "},
{"DIV_REM   "},
{"FILL      "},
{"AND       "},
{"OR        "},
{"XOR       "},
{"TEST      "},
{"LEN       "},
{"FORMAT    "},
{"MAK_FMT   "},
{"EDIT      "},
{"STRING    "},
{"SUBSTR    "},
{"INSTR     "},
{"UPPER     "},
{"PUSH      "},
{"POP       "},
{"SYNC_SAV  "},
{"SYNC_RELO "},
{"TIMER_ON  "},
{"WAIT      "},
{"START     "},
{"STOP      "},
{"SET_KEY   "},
{"SET_KEY   "},
{"SET_FUNC  "},
{"SET_FUNC  "},
{"CALL      "},
{"LINK      "},
{"RETURN    "},
{"TRANSFER  "},
{"EXIT      "},
{"GODEPEND  "},
{"ERROR     "},
{"SAVE_FIELD"},
{"SAVE_FIELD"},
{"RESTORE   "},
{"RELEASE   "},
{"CLEAR     "},
{"CLEAR     "},
{"NOTE      "},
{"POINT     "},
{"SOUND     "},
{"SOUND     "},
{"SORT      "},
{"LOOKUP    "},
{"SET_BCK_GR"},
{"TRIG_FUNC "},
{"FILESCREEN"},
{"SHOWSCREEN"},
{"UPLOAD    "},
{"DOWNLOAD  "},
{"ACCESS    "},
{"NOP       "},
{"TIMER_OFF "},
{"MOVE_BLOCK"},
{"RETURN    "},
{"EXIT      "},
{"REFRESH   "},
{"ERASE     "},
{"SET_CURSOR"},
{"TRACK     "},
{"LOG       "},
{"OPENERRWIN"},
{"DEFINE_FLD"},
{"SET_FUNC  "},
{"OPENERRWIN"},
};

unsigned char opnbrtbl[] =
{
              240,          /*BRK*/
              246,          /*CJEQ*/
              246,          /*CJNE*/
              246,          /*CJLT*/
              246,          /*CJGT*/
              246,          /*CJLE*/
              246,          /*CJGE*/
              243,          /*JMP*/
                5,          /*DEF_FLD*/
                6,          /* Def_fld_prg was 7 */
              249,          /*SET_ATT*/
                2,          /*OPEN*/
                0,          /*CLOSE_ALL*/
                1,          /*CLOSE*/
                2,          /*READ*/
                3,          /*READ_LINE*/
                2,          /*WRITE*/
                3,          /*WRITE_LINE*/
                1,          /*CONNECT*/
                0,          /*DISCONNECT*/
                   /* Driver 7 support - By C. H. 2/12/87  */
                1,          /* SEND_NO_ID , it was SNED_NO_RESP  2  */
                2,          /* SEND_YES_ID, it was  SEND_RESP    3   */
                2,          /*RECEIVE*/
                1,          /*CANCEL*/
                1,          /*NAV*/
                0,          /*NAVN*/
                0,          /*NAVB*/
                0,          /*Navigate FIRST*/
                0,          /*NAVIGATE LAST*/
                1,          /*FETCH*/
                2,          /*FETCHRQ*/
                1,          /*OPEN_WINDOW*/
                2,          /*OPEN_WINDOW_ERR*/
                0,          /*CLOSE_WINDOW*/
                1,          /*CLOSE & OPEN WINDOW*/
                1,          /*KILL*/
                0,          /*PURGE*/
                2,          /*MOVE*/
                2,          /*MOVE ABSOLUTE*/
                2,          /*SWAP */
                2,          /*ADD*/
                2,          /*SUB*/
                2,          /*MUL*/
                2,          /*DIV*/
                3,          /*DIV_REM*/
                3,          /*FILL*/
                2,          /*AND*/
                2,          /*OR*/
                2,          /*XOR*/
                2,          /*TEST */
                2,          /*LEN*/
                3,          /*FORMAT was 2*/
              244,          /*MAK_FMT*/
              245,          /*EDIT*/
              248,          /*STRING*/
                4,          /*SUBSTR*/
                3,          /*INSTR*/
                1,          /*UPPER*/
                1,          /*PUSH*/
                1,          /*POP*/
                3,          /*SYNC_SAV*/
                1,          /*SYNC_RELO*/
                1,          /*TIMER_ON*/
                1,          /*WAIT*/
                4,          /*START*/
                1,          /*STOP*/
                3,          /*SET_KEY*/
                5,          /*SET_KEY_PRGM*/
                2,          /*SET_FUNCTION*/
                3,          /*SET_FUNCTION_PRGM*/
              241,          /*CALL*/
              248,          /*LINK*/
              240,          /*RETURN*/
              248,          /* TRANSFER*/
              240,          /*EXIT*/
              242,          /*GO_TO_DEPEND*/
                1,          /*ERROR*/
              251,          /* SAVE FIELD */
                3,          /* SAVE FIELDS*/
                2,          /* RESTORE FIELDS. */
                1,          /* RELEASE FIELD. */
              252,          /* CLEAR FIELD. */
                2,          /* CLEAR_FIELDS. */
                2,          /* NOTE*/
                2,          /* POINT*/
                0,          /* sound*/
                2,          /* Set sound*/
                3,          /* Sort*/
                5,          /* LOOKUP*/
                1,          /* Set_background. */
                1,          /* TRIG_FUNC*/
                1,          /* FILE_SCREEN*/
                1,          /* SHOW_SCREEN*/
                2,          /* Upload*/
                2,          /* Download*/
                2,          /* ACCESS */
                0,          /* NOP      */
                1,          /* TIMER OFF*/
              251,          /* MOVE BLOCK */
                1,          /*RTN*/
                1,          /*EXIT*/
                0,          /* REFRESH*/
         1,          /*ERASE*/
         1,          /*SET_CURSOR */
         1,          /* TRACK */
         1,          /* LOG */
         1,          /* OPENERRWIN */
         7,             /*DEFINE FIELD*/
                4,        /*SET_FUNCTION_PRGM*/
         2,          /* OPENERRWIN */
};
